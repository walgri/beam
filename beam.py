from __future__ import division
import FreeCAD as App
import FreeCADGui as Gui

from obj import Beam, ViewProviderBeam


class make_beam(object):
    def __init__(self, view):
        self.profile = None
        self.midpoint = None
        self.path = None
        self.n = None
        self.view = view
        App.Console.PrintMessage("choose the profile\n")
        Gui.Selection.clearSelection()
        self.klick_event = self.view.addEventCallback("SoMouseButtonEvent", self.choose_profile)

    def choose_profile(self, cb):
        if cb["State"] == "DOWN":
            sel = Gui.Selection.getSelection()
            if len(sel) > 0:
                self.profile = sel[0]
                Gui.Selection.clearSelection()
                self.view.removeEventCallback("SoMouseButtonEvent", self.klick_event)
                self.klick_event = self.view.addEventCallback("SoMouseButtonEvent", self.choose_path)
                App.Console.PrintMessage("choose_path\n")

    def choose_path(self, cb):
        if cb["State"] == "DOWN":
            sel = Gui.Selection.getSelectionEx()
            if sel:
                path_sketch = sel[0].Object
                path_name = sel[0].SubElementNames[0]

                self.view.removeEventCallback("SoMouseButtonEvent", self.klick_event)

                a = App.ActiveDocument.addObject("Part::FeaturePython","beam")
                Beam(a, self.profile, path_sketch, path_name)
                ViewProviderBeam(a.ViewObject)
                App.ActiveDocument.recompute()


                App.Console.PrintMessage("end of tube tool\n")


if __name__ == "__main__":
    view = Gui.ActiveDocument.activeView()
    selection = make_beam(view)