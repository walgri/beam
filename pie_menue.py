import FreeCADGui as Gui
from PySide import QtGui, QtCore
import numpy as np

class PieMenue(QtGui.QWidget):
    def __init__(self, view, qview, command_list):
        super(PieMenue, self).__init__()
        self.setLayout(QtGui.QVBoxLayout(self))
        self._qview = qview
        self._qscene = qview.scene()
        self.parent = self._qscene
        self._view = view
        self._view.addEventCallback("SoKeyboardEvent", self.show)
        self.create_menue(command_list)
        self._proxy = self._qscene.addWidget(self)
        self.hide()
        self.i = 0

    def show(self, event):
        if event["Key"] == "TAB":
            if event["State"] == "DOWN" and self.isHidden():
                self.setVisible(True)
                pos = QtGui.QCursor.pos()
                print(pos)
                pos = self._qview.mapFromGlobal(pos)
                print(pos)
                pos = self._qview.mapToScene(pos)
                print(self._proxy.mapToScene(0, 0))
                self._proxy.setPos(pos)
            elif event["State"] == "DOWN" and not self.isHidden():
                self._show = False
                self.hide()

    def create_menue(self, command_list):
        for name, command in command_list:
            btn = MyButton(name, command, self)
            self.layout().addWidget(btn)



class MyButton(QtGui.QPushButton):
    def __init__(self, name, command, parent):
        super(MyButton, self).__init__(name, parent)
        self.clicked.connect(self.action)
        self.command = command

    def action(self):
        self.command()
        self.parent().hide()




mw = Gui.getMainWindow()
qview = mw.findChild(QtGui.QGraphicsView)
view = Gui.ActiveDocument.ActiveView
qscene = qview.scene()

def part_design():
    Gui.activateWorkbench("PartDesignWorkbench")

def part():
    Gui.activateWorkbench("PartWorkbench")

def draft():
    Gui.activateWorkbench("DraftWorkbench")

command_list = [
    ["PartDesign", part_design],
    ["Part", part],
    ["Draft", draft]]

piemenue = PieMenue(view, qview, command_list)

