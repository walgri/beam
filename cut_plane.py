from __future__ import division

import FreeCAD as App
import FreeCADGui as Gui
import Part

from obj import CBeam, ViewProviderPlaneCut


class make_plane_cut(object):
    def __init__(self, view):
        self.view = view
        App.Console.PrintMessage("choose a beam\n")
        self.klick_event = self.view.addEventCallback("SoMouseButtonEvent", self.choose_beam)

    def set_cut_obj(self, obj, beam, face):
        if not obj.cut_type:
            obj.cut_obj = beam
            obj.cut_type = "cut"
            obj.cut_obj_name = face
            return
        return False

    def choose_beam(self, cb):
        if cb["State"] == "DOWN":
            sel = Gui.Selection.getSelection()
            if len(sel) > 0:
                self.beam = sel[0]
                Gui.Selection.clearSelection()
                self.view.removeEventCallback("SoMouseButtonEvent", self.klick_event)
                self.klick_event = self.view.addEventCallback("SoMouseButtonEvent", self.choose_plane)
                App.Console.PrintMessage("choose acutting plane\n")

    def choose_plane(self, cb):
        if cb["State"] == "DOWN":
            sel = Gui.Selection.getSelectionEx()
            if len(sel) > 0:
                self.cut_beam = sel[0].Object
                self.cut_face = sel[0].SubElementNames[0]
                Gui.Selection.clearSelection()
                self.view.removeEventCallback("SoMouseButtonEvent", self.klick_event)

                a = App.ActiveDocument.addObject("Part::FeaturePython", "cut_beam")
                CBeam(a, self.beam)
                self.set_cut_obj(a, self.cut_beam, self.cut_face)
                ViewProviderPlaneCut(a.ViewObject)
                App.ActiveDocument.recompute()

if __name__ == "__main__":
    view = Gui.ActiveDocument.activeView()
    selection = make_plane_cut(view)
