import FreeCADGui as Gui
from pie_menue1 import PieDialog
from beam import make_beam
from cut_miter import make_miter_cut
from cut_plane import make_plane_cut
from cut_shape import make_shape_cut
from set_simple import set_simple


if __name__ == "__main__":
    view = Gui.ActiveDocument.ActiveView

    def beam(): make_beam(view)
    def miter(): make_miter_cut(view)
    def plane(): make_plane_cut(view)
    def shape(): make_shape_cut(view)

    command_list = [
        ["beam", beam],
        ["miter", miter],
        ["plane", plane],
        ["shape", shape],
        ["set_simple", set_simple]]

    beam_menue = PieDialog("b", command_list)
    view.addEventCallback("SoKeyboardEvent", beam_menue.showAtMouse)
